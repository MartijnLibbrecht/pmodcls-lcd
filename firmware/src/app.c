/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "bsp_config.h"
#include <stdio.h>
#include <conio.h>


// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************
#define chipselect PORTS_BIT_POS_9
#define BYTE unsigned char
// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback funtions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary local functions.
*/

uint32_t APP_ReadCoreTimer()
{
    uint32_t timer;

    // get the count reg
    asm volatile("mfc0   %0, $9" : "=r"(timer));

    return(timer);
}

void APP_StartTimer(uint32_t period)
{
    /* Reset the coutner */

    uint32_t loadZero = 0;

    asm volatile("mtc0   %0, $9" : "+r"(loadZero));
    asm volatile("mtc0   %0, $11" : "+r" (period));

}

void delay_us(uint32_t us) {
   unsigned coreTimer = APP_ReadCoreTimer();
   while ((APP_ReadCoreTimer() - coreTimer) < SYS_CLK_FREQ/1000000 * us);
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;
    
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */
char rx[40];
char txBuffer[40];
char tx;


void WriteTX(){
    
    // Zetten RB9 op 0 om daarna de data te sturen
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_B, chipselect);
    // Zend tx naar lcd
    txBuffer[0] = tx;
    DRV_SPI0_BufferAddWriteRead(txBuffer, rx, 1);     
    // Zetten RB9 op 1 om de data zending te stoppen
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_B, chipselect);
}

void sendBuffer(int length){
    
    // Zetten RB9 op 0 om daarna de data te sturen
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_B, chipselect);
    // Zend tx naar lcd
    DRV_SPI0_BufferAddWriteRead(txBuffer, rx, length);     
    // Zetten RB9 op 1 om de data zending te stoppen
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_B, chipselect);
}

void clearLCD(){
    // We zeggen dat het nog steeds 16 karakters per lijn zijn.
    txBuffer[0] = 0x1B;
    txBuffer[1] = '[';
    txBuffer[2] = '0';
    txBuffer[3] = 'h';
    sendBuffer(4);

    // Clear commando.
    txBuffer[0] = 0x1B;
    txBuffer[1] = '[';
    txBuffer[2] = 'j';
    sendBuffer(3);
}

void APP_Tasks ( void )
{
    /* Check the application's current state. */
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            // Toon aan de USART dat de controller geintitialiseerd is.
            printf("\r\nInitialized");
            // Toon aan de gebruiker dat de controller geintitialiseerd is.
            BSP_LEDOn(APP_USB_LED_1);
            BSP_LEDOn(APP_USB_LED_2);
            DRV_USART0_Initialize();
            
           
            //DRV_USART0_WriteByte("\r\nDone");
            appData.state = APP_STATE_RUN;
            break;
        }
        case APP_STATE_RUN:
        {
            // Zolang de karakterbuffer van de USART niet leeg is, voer uit
            if(!DRV_USART0_ReceiverBufferIsEmpty ()){
                // vraag de bytebuffer op
                
                tx = DRV_USART0_ReadByte();
                DRV_USART0_WriteByte(tx);
                
                //  Als het een * karakter is (voor in de uitbereiding)
                if(tx == '*'){
                    clearLCD();
                }
                else{
                    // Stuur het karakter naar de LCD
                    WriteTX();
                }
            }
            break;
        }
        /* TODO: implement your application state machine.*/

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}
 

/*******************************************************************************
 End of File
 */
